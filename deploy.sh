#!/bin/bash

set -ex

AWS_DEFAULT_REGION=ap-southeast-2
AWS_ACCOUNT_ID=581680512126


IMAGE_PREFIX=${AWS_ACCOUNT_ID}.dkr.ecr.${AWS_DEFAULT_REGION}.amazonaws.com
IMAGE_NAME=${IMAGE_PREFIX}/nginx

TAG=latest
IMAGE="$IMAGE_NAME:$TAG"

printf "Logging into AWS ECR\n"
eval $(aws ecr get-login --no-include-email --region "$AWS_DEFAULT_REGION") > /dev/null


docker build --pull -t "$IMAGE" .
docker push "$IMAGE"
