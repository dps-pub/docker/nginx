FROM nginx:alpine

LABEL maintainer="tom@dps.com.au"

# Install the AWS CLI
RUN apk --no-cache add python curl unzip && cd /tmp && \
    curl "https://s3.amazonaws.com/aws-cli/awscli-bundle.zip" \
    -o "awscli-bundle.zip" && \
    unzip awscli-bundle.zip && \
    ./awscli-bundle/install -i /usr/local/aws -b /usr/local/bin/aws && \
    rm awscli-bundle.zip && rm -rf awscli-bundle

COPY secrets-entrypoint.sh /secrets-entrypoint.sh

COPY entrypoint.sh /entrypoint.sh
COPY nginx.conf /etc/nginx/nginx.conf

ENTRYPOINT ["/secrets-entrypoint.sh"]
CMD ["/entrypoint.sh", "nginx"]
