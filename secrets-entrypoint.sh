#! /usr/bin/env sh

# Check that the environment variable has been set correctly
if [ -z "$SECRETS_BUCKET_NAME" ]; then
  echo >&2 'error: missing SECRETS_BUCKET_NAME environment variable'
  exit 1
fi

if [ -z "$ENVIRONMENT" ]; then
  echo >&2 'error: missing ENVIRONMENT environment variable'
  exit 1
fi

eval $(aws s3 cp s3://${SECRETS_BUCKET_NAME}/${ENVIRONMENT}/.env - | sed 's/^[#].*//' | sed '/^$/d' | sed 's/^/export /')

eval "$@"
